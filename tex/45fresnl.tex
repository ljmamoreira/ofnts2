\chapter{Eletrodinâmica da reflexão e refração da luz}
\label{chpt:fresnel}
\versiondate%
Uma vez que a luz é um fenómeno eletromagnético, devemos poder entender
\emph{todos} os fenómenos óticos no quadro da teoria eletromagnética.
Relativamente à reflexão e refração, temos que considerar a propagação da luz
num meio material não homogéneo, cujas propriedades óticas variam de ponto para
ponto. Mais precisamente, temos que considerar, na situação mais simples, dois
meios materiais homogéneos diferentes (bem, um deles pode ser o vácuo) e a
interface entre eles, a superfície onde as propriedades físicas da propagação da
luz sofrem descontinuidades. O procedimento em situações como
esta\footnote{Recordem, por exemplo, as aulas de Mecânica Quântica e a resolução
  da equação de Schrödinger 1D em potenciais uniformes por segmentos: o
degrau de potencial, a barreira e o poço retangular, etc.} consiste em resolver
as equações dinâmicas (neste caso, as eqs de Maxwell) em cada meio e impor
condições de continuidade na interface de separação dos dois meios. É com isso
que nos vamos ocupar agora mas, primeiro, revemos a teoria eletromagnética na
presença de meios materiais.

\section{Equações de Maxwell na presença de meios materiais}
As equações de Maxwell para o campo eletromagnético na presença de meios
materiais têm a forma, algo mais complicada que a das eqs.~\eqref{eq:maxweqs},
seguinte:
\begin{equation}
  \begin{aligned}\label{eq:maxweqs2}
    \div\vec D&=\rho_l & \div\vec B &=0\\
    \rot\vec E&=-\pd{\vec B}{t}&\rot\vec H&=\vec j_l+\pd{\vec D}{t},
  \end{aligned}
\end{equation}
Nestas equações, $\rho_l$ e $\vec j_l$ representam as densidades de carga livre
e corrente livre\footnote{As cargas e correntes livres são as fontes
  \emph{primeiras} dos campos elétricos e magnéticos. Têm estas designações para
  se distinguirem das \emph{cargas de polarização} e \emph{correntes de
  magnetização} que são \emph{induzidas} nos meios materiais pelos campos
  elétricos e magnéticos e para os quais também contribuem. O efeito destas
  cargas e correntes induzidas pode ser descrito através dos campos auxiliares
$\vec D$ e $\vec H$.}. Uma vez que, em geral, a reflexão e a refração se fazem
na ausência de cargas e correntes livres, vamos considerá-las ambas
identicamente nulas.  Os campos auxiliares \emph{deslocamento elétrico} $\vec D$
e \emph{campo $\vec H$}\footnote{Tradicionalmente, dava-se ao campo $\vec H$ o
  nome campo magnético e a $\vec B$ os nomes \emph{indução magnética} ou
  \emph{densidade de fluxo.} No entanto, o campo mais fundamental é o campo
  $\vec B$, e este campo desempenha no magnetismo o papel do campo $\vec E$ na
  eletricidade. Por isso, em muitos textos modernos, reserva-se para este campo
  o nome mais simples de \emph{campo magnético;} em contrapartida, o campo $\vec
  H$ tem várias designações. A que eu prefiro é a do Griffiths: campo $\vec H$,
simplesmente.}
ajudam na descrição dos efeitos da polarização e da magnetização do meio e são
dados por
\begin{align*}
  \vec D&=\varepsilon_0\vec E+\vec P&
  \vec H&=\frac{1}{\mu_0}\vec B-\vec M,
\end{align*}
onde $\vec P$ e $\vec M$ são respetivamente as densidades de momento dipolar
elétrico e de momento dipolar magnético induzidas no meio.  Em meios lineares e
isotrópicos,
$\vec D$ e $\vec H$ são,
respetivamente, proporcionais a $\vec E$ e $\vec B$:
\begin{align}\label{eq:lim}
  \vec D&=\epsilon \vec E& \vec H&=\frac{1}{\mu}\vec B,
\end{align}
onde $\epsilon$ é a permitividade elétrica e $\mu$ a permeabilidade magnética do
meio, que caracterizam as suas propriedades óticas e que nos meios não
homogéneos (ou na região de contacto de dois meios distintos) podem variar de
ponto para ponto.

\section{Condições fronteira em superfícies de descontinuidade}
A permitividade elétrica $\epsilon$ e a permeabilidade magnética $\mu$ podem
variar descontinuamente de um lado para o outro da superfície de contacto de
dois meios distintos. Em problemas em que estas condições se verificam,
as eqs. de Maxwell~\eqref{eq:maxweqs2} devem ser resolvidas em cada meio
material separadamente, e as soluções gerais obtidas devem depois ser ajustadas
através de condições fronteira impostas na superfície de contacto dos dois
meios. Estas condições fronteira podem ser deduzidas das equações de Maxwell,
por integração em domínios apropriados.

\subsection*{Componentes normais de $\vec D$ e $\vec B$}
\begin{minipage}[t]{0.7\linewidth}
Consideremos o integral da primeira das eqs.~\eqref{eq:maxweqs2} numa região com
a forma de um pequeno cilindro achatado com altura $\delta h$ (que faremos
tender para zero) e bases com área $\delta A$, cada uma em cada meio, com o eixo
perpendicular à superfície de contacto dos dois meios (ver a figura ao lado).
Usando o teorema do fluxo-divergência
\linebreak
\vspace{-0.7\baselineskip}
\end{minipage}\hfill
\begin{tikzpicture}[baseline=(current bounding box.north),
                    scale=0.7]
  \draw  (-1,1.1) -- (-3,-0.9) -- (2.0,-0.9) -- (2.5,1.1) -- cycle;
  \draw [thick] (0.2,0.2) ellipse (1 and 0.3);
  \draw [thick,->] (0.2,0.2) --+(0,0.55) node[right]{$\hat n$};
  \draw [thick] (-0.8,0.2) -- (-0.8,0);
  \draw [thick] (1.2,0.2) -- (1.2,0);
  \draw (-0.8,0) arc(180:360:1 and 0.32);
  \draw [densely dashed] (-0.8,0) -- (-0.8,-0.2);
  \draw [densely dashed] (1.2,0) -- (1.2,-0.2);
  \draw [densely dashed] (-0.8,-0.2) arc(180:360:1 and 0.34);
\end{tikzpicture}\linebreak
(teorema de Gauss),
temos, para o primeiro membro,
\begin{equation*}
  \int\limits_V\div\vec D\,dV = \int\limits_{S(V)}\vec D\cdot\hat ndS
  =\int\limits_{S_+}\vec D\cdot\hat n dS+
  \int\limits_{S_-}\vec D\cdot\hat n dS+
  \int\limits_{SL}\vec D\cdot\hat n dS,
\end{equation*}
onde $S_\pm$ representa as superfícies das bases do volume cilíndrico,
respetivamente do lado para onde $\hat n$ aponta ($+$) e do lado contrário
($-$), e $SL$ representa a superfície lateral. Mas, no limite $\delta
h\rightarrow0$, a área de $SL$ anula-se e o terceiro integral é nulo. Os outros
dois, para $\delta A\simeq0$, são aproximadamente dados por 
\begin{equation*}
  \int\limits_{S\pm} \vec D\cdot\hat n \rightarrow \pm\hat n\cdot \vec
  D_\pm\delta A,
\end{equation*}
onde $\vec D_\pm$ representam o deslocamento elétrico nos centros das
superfícies $S_\pm$, respetivamente.\footnote{O sinal $\pm$ resulta de a normal $\hat n$
da superfície $S_-$ ser simétrica da de $S_+$.} Assim, o integraldo primeiro
membro da primeira das eqs.~\eqref{eq:maxweqs2} pode escrever-se como
\begin{equation*}
  \int_V\div\vec D\, dV=\hat n\cdot\left(\vec D_+-\vec D_-\right)\delta A.
\end{equation*}
Quanto ao segundo membro, devemos entender o integral de volume da densidade de
carga livre como a \emph{carga total} presente no interior do volume
considerado, incluindo a carga total da distribuição espacial com densidade
espacial $\rho_l$ (que é nula no limite $\delta h\rightarrow0$), mais a carga
total de uma distribuição superficial definida na parte da superfície de
contacto dois dois meios situada no interior do volume de integração, com
densidade superficial $\sigma_l$.\footnote{E, se houver cargas livres pontuais ou
distribuídas linearmente a considerar deverão também ser tomadas em linha de
conta, claro.} Assim, no limite $\delta h\rightarrow 0$, temos
\begin{equation*}
  \int\limits_V\rho_l dV \rightarrow \sigma_l\delta A.
\end{equation*}
Juntando ao resultado anterior, obtemos a condição fronteira para as componentes
normais do deslocamento elétrico na superfície de dois meios diferentes:
\begin{equation*}
\hat n\cdot(\vec D_+-\vec D_-) = \sigma_l.
\end{equation*}
Os produtos escalares $\vec D_\pm\cdot \hat n$ são respetivamente iguais às
componente dos campos $\vec D_\pm$ na direção perpendicular à
superfície de contacto; representando essas componentes por 
\begin{equation*}
  D_\pm^\perp=\vec D_\pm\cdot\hat n,
\end{equation*}
podemos reescrever a condição fronteira que acabámos de deduzir na forma
\begin{equation}
  D_+^\perp-D_-^\perp = \sigma_l.
\end{equation}
De modo semelhante, por integração da segunda das eqs.~\eqref{eq:maxweqs2}
deduz-se a condição fronteira para a componente normal de $\vec B$:
\begin{equation}
  B_+^\perp-B_-^\perp=0.
\end{equation}

\subsection*{Componentes tangenciais de $\vec E$ e $\vec H$}
\noindent
\begin{minipage}[t]{0.70\linewidth}
  \hspace{\indentsp}
  As condições fronteira para as componentes tangenciais obtém\-{-se} por
  cálculo do fluxo da terceira e quarta das eqs.~\eqref{eq:maxweqs2} (as que
  determinam o rotacional de $\vec E$ e de $\vec H$) atra\-vés de uma superfície
  retangular pequena, com comprimento $\delta L$ e largura $\delta h$, com o
  comprimento paralelo à superfície de contacto dos dois meios.
  \linebreak
  \vspace{-0.6\baselineskip}
\end{minipage}
\hfill
\begin{tikzpicture}[baseline=(current bounding box.north),
                    scale=0.7]
  \small
  \draw  (-1,1.1) -- (-3,-1) -- (2.0,-1) -- (2.5,1.1) -- cycle;
  \draw [thick] (-0.6,0.6) coordinate(a) --
						 ++(0,0.3) coordinate (c)--
						 ++(-20:1.8) coordinate(d) -- 
						 ++(0,-0.6) coordinate(b);
  \fill[gray!40,opacity=0.4] (a) -- (c) -- (d) -- (b) -- cycle;
  \draw [thick,dashed] (b) -- 
						 ++(0,-0.6) coordinate(e)
							-- ([yshift=-0.4cm]a) coordinate(f) -- (a);
  \fill[gray!60,opacity=0.4] (b) -- (e) --(f) -- (a) -- cycle;
  \draw [thick,->](d) -- +(160:.5)
    node [above,xshift=15]{$\vec dl=\hat t\,dl$};
  \draw [thick,->] (0.3,0.14) -- +(200:1)
    node[left, inner sep=1]{$\hat s$};
  \draw [thick,->] (0.3,0.14) --+(0,1) node[left,fill=white,inner
    sep=1,xshift=-4]{$\hat n$};
\end{tikzpicture}
Para o campo $\vec E$ temos
\begin{equation*}
  \int\limits_S\rot\vec E\cdot \hat s\, dS =
  -\int\limits_S\pd{\vec B}{t}\cdot \hat s\, dS.
\end{equation*}
De acordo com o teorema de Stokes, o fluxo do rotacional de $\vec E$ é a
circulação do campo ao longo do caminho que delimita a superfície. No limite
$\delta h\rightarrow0$, somente os comprimentos (os lados paralelos à interface
dos dois meios) do retângulo contribuem para a circulação, pelo que 
\begin{equation*}
  \int_S\rot\vec E\cdot\hat sdS=\hat t\cdot
  \left(\vec E_+-\vec E_-\right)\delta L
\end{equation*}
Por outro lado, o fluxo da derivada temporal do campo magnético anula-se no
limite $\delta h\rightarrow0$, porque se anula a área. Resulta então
\begin{equation*}
  \hat t\cdot(\vec E_+ - \vec E_-)=0,
\end{equation*}
qualquer que seja $\hat t$ tangente à superfície de contacto dos dois meios.
Esta igualdade verifica-se qualquer que seja a orientação azimutal da superfície
de integração (mantendo-se perpendicular à superfície de contacto dos dois
meios, bem entendido); isto é, qualquer que sejam as orientações escolhidas para
os versores $\hat s$ e $\hat t$. Assim, ela pode ser entendida como a igualdade
das projeções dos dois limites laterais do campo no plano tangente à superfície
de descontinuidade,
\begin{equation}
  \vec E_+^\parallel-\vec E_-^\parallel =0.
\end{equation}

Do mesmo modo (mas tendo em consideração que na superfície pode estar definida
uma distribuição superficial de corrente livre $\vec\kappa_l$, que tem um fluxo
não nulo mesmo no limite $\delta h\rightarrow0$, obtem-se para as componentes
tangenciais do campo $\vec H$ a condição
\begin{equation}
  \hat t\cdot(\vec H_+-\vec H_-)=\vec\kappa_l\cdot \hat s.
\end{equation}
Mas note que $\hat s=\hat n\times\hat t$ e que 
\begin{equation*}
\hat s\cdot\vec \kappa_l=
(\hat n\times\hat t)\cdot\vec\kappa_l=
(\vec\kappa_l\times\hat n) \cdot\hat t,
\end{equation*}
de onde resulta
\begin{equation}
  \hat t\cdot(\vec H_+-\vec H_-)= \hat t\cdot\vec\kappa_l\times \hat n,
\end{equation}
ou seja,
\begin{equation}
  \vec H^\parallel_+-
  \vec H^\parallel_-=\vec \kappa_l\times\hat n.
\end{equation}

Em resumo, as condições fronteira que oss campos $\vec E$, $\vec B$, $\vec D$ e
$\vec H$ devem satisfazer nos pontos de superfícies de descontinuidade, onde
estejam definidas distribuições superficiais de carga e/ou corrente
livres com densidades $\sigma_l$ e $\vec\kappa_l$, são
\begin{equation}\label{eq:boundc}
  \begin{aligned}
    D_+^\perp-D_-^\perp&=\sigma_l&
    B_+^\perp-B_-^\perp&=0\\
    \vec E_+^\parallel-\vec E_-^\parallel&=0&
    \vec H_+^\parallel-\vec H_-^\parallel&=\vec\kappa\times\hat n&
  \end{aligned}
\end{equation}

\section{Leis da reflexão e refração}
Consideremos dois meios materiais lineares, homogéneos e isotrópicos diferentes
(ou seja, com diferentes permitividade elétrica e permeabilidade magnética), em
contacto numa superfície $S$ que vamos considerar plana\footnote{Na análise que
  se segue (como, aliás, na que fizémos até agora), nada de essencial depende da forma da superfície de contacto dos
  dois meios (desde que seja contínua e diferenciável, ou seja, desde que não
  tenha arestas), apenas se evita a necessidade de usar expressões como ``plano
  tangente à superfície de contacto'' em vez de, simplesmente, ``superfície de
contacto.'' A conveniência disto é, para mim, justificação bastante.}.
Consideremos ainda que num ponto muito afastado da superfície de contacto dos
dois meios, uma fonte gera uma onda eletromagnética plana e sinusoidal que
incide na superfície (a onda incidente), e que não há outras fontes a complicar
esta situação.  Sejam $\vec E_{0i}$, $\vec k_i$ e $\omega_i$ respetivamente a
amplitude do campo elétrico, o vetor de onda e a frequência angular desta onda.
A incidência desta onda na superfície gera duas novas ondas. Uma, a onda
refletida, propaga\--se no mesmo meio que a incidente; a outra propaga-se no
segundo meio, e vamos chamar-lhe onda \emph{transmitida.}\footnote{Um nome mais
  apropriado seria \emph{onda refratada.} Usamos a designação indicada só porque
  a sua inicial é diferente da da que escolhemos para a onda refletida, o que
  facilita a notação.} Os vetores campo elétrico das três ondas podem
  escrever\-\mbox{-se} respetivamente como 
\begin{align}\label{eq:elfw_irt}
  \vec E_i(\vec r, t)&=\vec E_{0i}e^{i(\vec k_i\cdot \vec r-\omega_i t)}&
  \vec E_r(\vec r, t)&=\vec E_{0r}e^{i(\vec k_r\cdot \vec r-\omega_r t)}&
  \vec E_t(\vec r, t)&=\vec E_{0t}e^{i(\vec k_t\cdot \vec r-\omega_t t)},
\end{align}
onde os vários símbolos têm os significados habituais. Note que as amplitudes
$\vec E_{0i}$, etc, podem ter parte imaginária, dando conta de eventuais
constantes de fase em cada uma das ondas.  O campo elétrico no meio onde se
propagam as ondas incidente e refletida (o \emph{meio de incidência}) é, em cada
ponto e em cada instante, a soma dos campos destas duas ondas; no outro
meio (o de \emph{refração,} apenas há a considerar o da onda transmitida:
\begin{align*}
  \vec E_1(\vec r, t)&=\vec E_i(\vec r, t) + \vec E_r(\vec r, t)&
  \vec E_2(\vec r, t)&=\vec E_t(\vec r, t).
\end{align*}
Na superfície de contacto dos dois meios, as componentes normais e tangenciais
do campo elétrico devem satisfazer as condições fronteira das
eqs~\eqref{eq:boundc}. Supondo que não há distribuições de cargas livres ou de
correntes livres definidas na superfície e tendo em conta as
relações~\eqref{eq:lim}, essas condições reduzem-se a
\begin{equation}\label{eq:bdrconds}
\begin{aligned}
  \epsilon_1
  \left(
    E^\perp_{0i}e^{i(\vec k_i\cdot\vec r-\omega_it)}+
    E^\perp_{0r}e^{i(\vec k_r\cdot\vec r-\omega_rt)}
  \right)&=
  \epsilon_2E^\perp_{0t}e^{i(\vec k_t\cdot\vec r-\omega_tt)}\\
  \vec E^\parallel_{0i}e^{i(\vec k_i\cdot\vec r-\omega_it)}+
  \vec E^\parallel_{0r}e^{i(\vec k_r\cdot\vec r-\omega_rt)}&=
  \vec E^\parallel_{0t}e^{i(\vec k_t\cdot\vec r-\omega_tt)},
\end{aligned}
\qquad
\text{ na interface.}
\end{equation}
onde $\epsilon_1$ e $\epsilon_2$ são, respetivamente, as permitividades
elétricas do meio de incidência e do de refração. Esta expressão é válida nos
pontos da superfície de contacto dos dois meios, pelo que $\vec r$ representa
aqui o vetor posição de um ponto arbitrário desta superfície. Escolhendo a
origem do sistema de coordenadas também na interface, $\vec r$ é então um vetor
paralelo à superfície.\footnote{Este facto será usado mais adiante.  Mas é
  possível chegar às mesmas conclusões com sistemas de coordenadas centrados
fora da interface.}
Note que estas igualdades devem verificar-se em \emph{qualquer} ponto do plano
de contacto e em \emph{qualquer} instante. Estas condições impõem relações entre
as componentes normal e tangencial do campo elétrico nos dois meios, mas impõem
para além disso que os expoentes das exponenciais sejam iguais, ou seja,
\begin{equation*}
  \vec k_i\cdot\vec r-\omega_it=
  \vec k_r\cdot\vec r-\omega_rt=
  \vec k_t\cdot\vec r-\omega_tt,\qquad\forall\vec r \in S.
\end{equation*}
Mais ainda, como o tempo $t$ é uma variável independente da posição, devem
verificar-se igualdades independentes dos termos temporais e
espaciais,\footnote{Se estas conclusões não parecem óbvias ou se as quiser ver
demonstradas de forma mais formal, veja o apêndice no final deste capítulo.}
\begin{align*}
  \omega_i= \omega_r&= \omega_t, &
  \vec k_i\cdot\vec r&=
  \vec k_r\cdot\vec r=
  \vec k_t\cdot\vec r.
\end{align*}
As igualdades à esquerda revelam que as frequências das três ondas são todas
iguais (passamos a representar o valor comum por $\omega$); as da direita, que
são iguais as componentes tangenciais à superfície de contacto dos três vetores
de onda. Destas últimas deduz-se que 
\begin{align*}
  (\vec k_i-\vec k_r)\cdot \vec r&=0 &
  (\vec k_i-\vec k_t)\cdot \vec r&=0, \qquad\forall \vec r\in S,
\end{align*}
ou seja, que os vetores $\vec k_i-\vec k_r$ e $\vec k_i-\vec k_t$ são
perpendiculares à superfície. Logo, os seus produtos vetoriais com a normal
$\hat n$ anulam-se:
\begin{align*}
  (\vec k_i-\vec k_r)\times\hat n&=0,&
  (\vec k_i-\vec k_t)\times\hat n&=0.
\end{align*}
Estas duas igualdades mostram que os vetores de onda das ondas refletida e
transmitida são coplanares com o vetor de onda da onda incidente e com a normal
à superfície. Este plano que contem as três direções de propagação e ainda a
normal chama-se \emph{plano de incidência.}
Considerando os módulos dos produtos vetoriais nestas igualdades, tem-se
\begin{align*}
  k_i\sin\theta_i&=k_r\sin\theta_r&
  k_i\sin\theta_i&=k_t\sin\theta_t,
\end{align*}
onde $\theta_i$, $\theta_r$ e $\theta_t$ representam, respetivamente, os ângulos
entre os vetores $\vec k_i$, $\vec k_r$ e $\vec k_t$ e a normal, isto é, os
ângulos de incidência, de reflexão e de transmissão.\footnote{AKA refração.} Mas
note agora que $k=\omega/v=n\omega/c$, onde $v$ é o módulo da velocidade de
propagação da luz num meio material, $c$ o valor para a propagação no vácuo e
$n$ o índice de refração no meio. Substituindo na primeira das igualdades acima
(que relaciona os vetores de onda de ondas que se propagam no mesmo meio),
resulta $\sin\theta_i=\sin\theta_r$, ou seja, a lei da reflexão,
\begin{equation*}
   \theta_i=\theta_r.
\end{equation*}
Fazendo a mesma substituição na segunda igualdade, mas tendo em conta que se
trata de ondas que se propagam em meios diferentes, obtemos a lei de Snell,
\begin{equation*}
  n_1\sin\theta_i=n_2\sin\theta_t,
\end{equation*}
onde $n_1$ e $n_2$ representam os índices de refração do meio de incidência e de
refração, respetivamente.
Concluímos assim que as leis da reflexão e da refração são apenas uma
consequência das condições fronteira impostas aos campos nas superfícies de
descontinuidade. Na verdade, nem isso. Mesmo que a forma concreta das condições
fronteira fosse diferente, envolvendo diferentes combinações lineares dos campos
nos dois meios, mesmo assim seria necessário para as satisfazer que as
exponenciais nas três ondas (incidente, refletida e transmitida) fossem iguais
em todos os pontos da superfície de contacto dos dois meios e em todos os
instantes. Logo, seriam também válidos os passos que nos trouxeram às leis
da reflexão e da refração. Isto mostra que estes fenómenos devem manifestar-se
com outros tipos de ondas, não apenas com as eletromagnéticas.

Mas há ainda outras consequências das condições fronteira, que vão para além do
que normalmente é estudado em ótica geométrica.



\section{Equações de Fresnel}
Voltemos agora às condições fronteira das eqs.~\eqref{eq:bdrconds}. Uma vez que
as exponenciais são todas iguais, podemos eliminá-las das igualdades, restando
apenas condições sobre as amplitudes dos campos elétricos das três ondas:
\begin{align}
  \epsilon_1 \left( E^\perp_{0i}+ E^\perp_{0r} \right)&= \epsilon_2E^\perp_{0t}&
  \vec E^\parallel_{0i}+ \vec E^\parallel_{0r}&= \vec E^\parallel_{0t}.
\end{align}
Do mesmo modo e pelas mesmas razões, as condições fronteira para o campo
magnético resultam em
\begin{align}
  B_{0i}^\perp+B_{0r}^\perp&=B_{0t}^\perp&
  \frac{1}{\mu_1}\left(\vec B_{0i}^\parallel+\vec B_{0r}^\parallel\right)&=
  \frac{1}{\mu_2}\vec B_{0t}^\parallel
\end{align}

É muito conveniente agora introduzir uma base cartesiana para facilitar a
visualização e a descrição da orientação dos campos elétrico e magnético nas
três ondas. Escolhemos o plano $xy$ coincidente com o plano de
contacto dos dois meios com o eixo dos $y$ na direção da intersecção desse plano
com o plano de incidência (ver a Figura~\ref{fig:fresnelaxes}).
\begin{figure}[htb]
  \centering
    \includegraphics{figs/f45-10.pdf}
    \caption{\label{fig:fresnelaxes}
      Direções dos eixos cartesianos usados na análise e das componentes 
      perpendiculares ($\vec E_x$) e paraelelas ($\vec E_p$) ao plano de
      incidência dos campos elétricos das ondas incidente, refletida e
    transmitida.}
\end{figure}
Relativamente a esta base, as componentes perpendiculares dos campos são as
componentes $z$ e as paralelas, as componentes $x$ e $y$. As condições fronteira
acima podem então escrever-se como
\begin{equation}\label{eq:beqs1}
  \begin{aligned}
    E_{ix} + E_{rx}&=E_{tx} & 
    \frac{1}{\mu_1}\left(B_{ix}+B_{rx}\right)&=\frac{1}{\mu_2}B_{tx}\\
    E_{iy} + E_{ry}&=E_{ty} & 
    \frac{1}{\mu_1}\left(B_{iy}+B_{ry}\right)&=\frac{1}{\mu_2}B_{ty}\\
    \varepsilon_1\left(E_{iz}+E_{rz}\right)&=\varepsilon_2E_{tz} &
    B_{iz}+B_{rz}&=B_{tz},
  \end{aligned}
\end{equation}
onde se abandonou o índice ``0''\footnote{Que indica que se está a referir a
\emph{amplitude} do campo elétrico e não o campo elétrico propriamente dito.
Mas, nesta fase, já só estamos a discutir as amplitudes dos campos elétrico e
magnético e, portanto, é redundante essa indicação.} para não sobrecarregar a
notação.

Relativamente a esta base, os vetores de onda das ondas incidente, refletida e
transmitida escrevem-se como
\begin{align*}
  \vec k_i&=k_i(0, \sin\theta_1, -\cos\theta_1) & 
  \vec k_r&=k_i(0, \sin\theta_1,  \cos\theta_1) & 
  \vec k_t&=k_i(0, \sin\theta_2, -\cos\theta_2).
\end{align*}
Por fim, as amplitudes do campo magnético de cada onda podem ser
calculadas a partir das do campo elétrico correspondente, usando a
eq.~\eqref{eq:mfa}, que aqui reescrevemos como
\begin{equation}
  \vec B_0=\frac{k}{\omega}\,\hat k\times\vec E_0=\sqrt{\varepsilon\mu}\;
  \hat k\times\vec E_0=\frac{1}{c}\hat k\times\vec E_0,
\end{equation}
onde $c=1/\sqrt{\varepsilon\mu}$ é a velocidade de propagação da luz, no meio
onde essa propagação se faz (o meio de incidênca para os raios incidente e
refletido, o de refração para o raio transmitido).  Podem, então ser escritas
como
\begin{align*}
  \vec B_{0i}&=\frac{1}{c_1}
    \left(\sin\theta_1 E_{iz}+\cos\theta_1 E_{iy}, 
    -\cos\theta_1 E_{ix}, -\sin\theta_1 E_{ix}\right)\\
  \vec B_{0r}&=\frac{1}{c_1}
    \left(\sin\theta_1 E_{rz}-\cos\theta_1 E_{ry}, 
    \cos\theta_1 E_{rx}, -\sin\theta_1 E_{rx}\right)\\
  \vec B_{0t}&=\frac{1}{c_2}
    \left(\sin\theta_2 E_{tz}+\cos\theta_2 E_{ty}, 
    -\cos\theta_2 E_{tx}, -\sin\theta_2 E_{tx}\right)
\end{align*}
As condições fronteira das eqs.~\eqref{eq:beqs1} podem então escrever-se como
\begin{gather*}
  E_{ix}+E_{rx} = E_{tx}  \qquad
  E_{iy}+E_{ry} = E_{ty} \qquad
  \varepsilon_1\left(E_{iz}+E_{rz}\right)=\varepsilon_2E_{tz}\\
  \varepsilon_1c_1
    \left(\sin\theta_1\left[E_{iz}+E_{rz}\right]
    +\cos\theta_1\left[E_{iy}-E_{ry}\right]\right)=
    \varepsilon_2c_2
    \left(\sin\theta_2E_{tz}+\cos\theta_2E_{ty}\right)\\
  \varepsilon_1c_1
    \cos\theta_1\left(E_{ix}-E_{rx}\right)=
    \varepsilon_2c_2\cos\theta_2E_{tx}\\
    \frac{1}{c_1}
    \sin\theta_1\left(E_{ix}+E_{rx}\right)=
    \frac{1}{c_2}\sin\theta_2E_{tx}.
\end{gather*}
Note-se agora que as componentes $y$ e $z$ dos campos elétricos (as
componentes que pertencem ao plano de incidência) não são independentes, uma vez
que o campo elétrico em cada onda deve ser perpendicular à direção de
propagação, ou seja, à direção de incidência (para o $\vec E_i$), à de reflexão
(para o $\vec E_r$) e à de refração (para o $\vec E_t$). Assim, para a amplitude
do campo elétrico da onda incidente, temos
\begin{align*}
  E_{iy}&=E_{ip}\cos\theta_1& E_{iz}&=E_{ip}\sin\theta_1 & 
  E_{ip}&=\sqrt{E_{iy}^2+E_{iz}^2},
\end{align*}
onde $E_{ip}$ representa o módulo da projeção da amplitude do campo elétrico da
onda incidente no plano de incidência. Do mesmo modo, para a onda refletida e
transmitida,
\begin{align*}
  E_{ry}&=-E_{rp}\cos\theta_1&
  E_{rz}&=E_{rp}\sin\theta_1\\
  E_{ty}&=E_{tp}\cos\theta_2&
  E_{tz}&=E_{tp}\sin\theta_2
\end{align*}
Substituindo em cima e reordenando, resultam as igualdades seguintes:
\begin{equation}\label{eq:bdrcs}
  \begin{aligned}
    E_{ix} + E_{rx} &= E_{tx}\\
    \frac{1}{c_1\mu_1}\cos\theta_1\left(E_{ix} - E_{rx}\right)&=
    \frac{1}{c_2\mu_2}\cos\theta_2E_{tx}\\
      \frac{1}{c_1}\sin\theta_1\left(E_{ix}-E_{rx}\right)&=
      \frac{1}{c_2}\sin\theta_2E_{tx}\\
    \varepsilon_1\sin\theta_1\left(E_{ip}+E_{rp}\right)&=
      \varepsilon_2\sin\theta_2E_{tp}\\
    \frac{1}{c_1\mu_1}\left(E_{ip}+E_{rp}\right)&=
    \frac{1}{c_2\mu_2}E_{tp}\\
    \cos\theta_1\left(E_{ip}-E_{rp}\right)&=\cos\theta_2E_{tp}
  \end{aligned}
\end{equation}

Conhecida a amplitude do campo elétrico da onda incidente,  estas seis equações
podem então ser resolvidas para determinar as componentes das amplitudes das
ondas refletida (onda-$r$) e refratada (onda-$t$, de \emph{transmitida}). Mas a
análise é mais simples (ou melhor, menos complicada) se considerarmos
separadamente dois estados de polarização da onda incidente, um perpendicular, o
outro paralelo ao plano de incidência. Note-se que ao focar a nossa análise
nestes dois casos não estamos verdadeiramente a perder generalidade, uma vez
que, como já vimos, qualquer estado de polarização se pode escrever como a
sobreposição de duas ondas polarizadas linearmente em direções perpendiculares.

\subsection*{Caso 1: $\vec E_{i}$ perpendicular ao plano de incidência
(polarização ``S''\footnote{Da palavra alemã ``senkrecht'' para ``perpendicular''.})}
Nesta situação, temos $E_{ip}=0$ (ou seja, $E_{iy}=E_{iz}=0$). 
Das três últimas
equações~\eqref{eq:bdrcs} resulta que os campos elétricos das ondas refletida e
transmitida são igualmente perpendiculares ao plano de incidência, ou seja,
\begin{equation*}
  E_{rp}=E_{tp}=0
\end{equation*}
Por outro lado, usando a primeira das equações~\eqref{eq:bdrcs} para eliminar da
segunda $E_{tx}$, resulta
\begin{align*}
  \left(\frac{E_{rx}}{E_{ix}}\right)_\text{S} &=
    \frac{%
      \frac{1}{c_1\mu_1}\cos\theta_1-
      \frac{1}{c_2\mu_2}\cos\theta_2}{%
      \frac{1}{c_1\mu_1}\cos\theta_1+
      \frac{1}{c_2\mu_2}\cos\theta_2},
\end{align*}
ou, em termos dos índices de refração dos dois meios $n_1=c/c_1$, $n_2=c/c_2$,
\begin{align*}
  \left(\frac{E_{rx}}{E_{ix}}\right)_\text{S} &=
    \frac{%
      \frac{n_1}{\mu_1}\cos\theta_1 - \frac{n_2}{\mu_2}\cos\theta_2}{%
      \frac{n_1}{\mu_1}\cos\theta_1 + \frac{n_2}{\mu_2}\cos\theta_2}
\end{align*}
Do mesmo modo, eliminando agora $E_{rx}$, obtemos
\begin{align*}
  \left(\frac{E_{tx}}{E_{ix}}\right)_\text{S} &=
    \frac{%
      2\frac{n_1}{\mu_1}\cos\theta_1}{%
      \frac{n_1}{\mu_1}\cos\theta_1 + \frac{n_2}{\mu_2}\cos\theta_2}
\end{align*}
Nas situações de reflexão e refração da luz normalmente consideradas, os valores
das permeabilidades magnéticas dos dois meios envolvidos são muito próximos (e
ambos próximos do valor característico do vácuo, $\mu_0$). Estas equações podem
nesse caso ser escritas como
\begin{align}\label{eq:frsnls}
  \left(\frac{E_{rx}}{E_{ix}}\right)_\text{S} &=-
    \frac{\sin(\theta_1-\theta_2)}{\sin(\theta_1+\theta_2)}&
  \left(\frac{E_{tx}}{E_{ix}}\right)_\text{S} &=
    \frac{2\sin\theta_1\sin\theta_2}{\sin(\theta_1+\theta_2)}&
\end{align}


\subsection*{Caso 2: $\vec E_{i}$ paralelo ao plano de incidência
(polarização ``P''\footnote{De ``paralela''.})}
Agora temos $E_{ix}=0$. As primeiras três equações~\eqref{eq:bdrcs} têm como
solução $E_{rx}=E{tx}=0$, ou seja, que as amplitudes dos campos elétricos da
onda refletida e transmitida também são paralelas ao plano de incidência. Usando
agora as duas últimas equações~\eqref{eq:bdrcs}, obtemos
\begin{align*}
  \left(\frac{E_{rp}}{E_{ip}}\right)_\text{P} &=
  \frac{\frac{n_2}{\mu_2}\cos\theta_1-\frac{n_1}{\mu_1}\cos\theta_2}%
  {\frac{n_2}{\mu_2}\cos\theta_1+\frac{n_1}{\mu_1}\cos\theta_2}&
  \left(\frac{E_{tp}}{E_{ip}}\right)_\text{P} &=
  \frac{2\frac{n_1}{\mu_1}\cos\theta_1}%
       {\frac{n_2}{\mu_2}\cos\theta_1+\frac{n_1}{\mu_1}\cos\theta_2}
\end{align*}
No caso de as permeabilidades magnéticas serem iguais, resulta
\begin{align}\label{eq:frsnlq}
  \left(\frac{E_{rp}}{E_{ip}}\right)_\text{P} &=
  \frac{n_2\cos\theta_1-n_1\cos\theta_2}%
  {n_2\cos\theta_1+n_1\cos\theta_2}&
  \left(\frac{E_{tp}}{E_{ip}}\right)_\text{P} &=
  \frac{2n_1\cos\theta_1}%
       {n_2\cos\theta_1+n_1\cos\theta_2},
\end{align}
igualdades que podem também ser expressas como (explico o truque na
Secção~\ref{sect:dphi})
\begin{align}\label{eq:frsnlp}
  \left(\frac{E_{rp}}{E_{ip}}\right)_\text{P} &=
    \frac{\tan(\theta_1-\theta_2)}{\tan(\theta_1+\theta_2)}&
  \left(\frac{E_{tp}}{E_{ip}}\right)_\text{P} &=
    \frac{2\cos\theta_1\sin\theta_2}%
    {\sin(\theta_1+\theta_2)\cos(\theta_1-\theta_2)}
\end{align}

As igualdades~\eqref{eq:frsnls} e~\eqref{eq:frsnlp}, que permitem calcular as
amplitudes das ondas refletida e refratada conhecida a da onda incidente,
chamam-se \emph{equações de Fresnel.} Com elas podemos estabelecer numa base
teórica uma série de observações empíricas, algumas das quais já encontrámos no
estudo da Ótica Física: a reflexão total, a variação de fase nas reflexões, a
influência na polarização da onda refletida, a intensidade relativa da onda
refletida e refratada, etc.

\subsection{Variação de fase na reflexão}
Se o índice de refração do meio de refração for maior do que o de incidência,
então, de acordo com a lei de Snell, o ângulo de refração é menor que o de
incidência. Considerando o caso ``S'' e a primeira das eqs.~\eqref{eq:frsnls},
constatamos que as amplitudes da onda refletida e incidente têm sinais opostos,
ou seja, representam ondas em oposição de fase. Não se verifica isso se
$n_2<n_1$. Ou seja, a amplitude da componente de polarização normal ao plano de
incidência (ou seja, paralela ao plano de separação dos dois meios) da onda
refletida tem sentido oposto ao da onda incidente, se o índice de refração do
meio de refração ($n_2$, nesta dedução) for maior do que o do meio de
incidência ($n_1$). Mas trocar o sentido do vetor amplitude de uma onda é
equivalente a acrescentar $\pi$ (180$^\circ$) à sua fase. Ou seja, se $n_2>n_1$,
a componente de polarização paralela ao plano de separação dos dois meios da
onda refletida tem a fase da mesma componente da onda incidente, acrescentada de
$\pi$.

Quando a incidência se faz perpendicularmente ao plano de separação dos dois
mios, o plano de incidência não está definido. Mas, nesse caso, a polarização da
onda incidente é paralela ao plano de separação. Assim, verifica-se também
esta inversão de fase da onda refletida quando $n_2>n_1$, para a incidência
normal. Este facto é muito importante em considerações sobre interferência na
reflexão em filmes finos.

\subsection{Ângulo de Brewster}
As amplitudes das duas componentes das ondas refletidas e transmitidas dependem
fortemente do ângulo de incidência. É especialmente interessante o caso da
amplitude da polarização paralela ao plano de incidência da onda refletida,
quando $\theta_1+\theta_2\rightarrow\pi/2$. Nesse limite, o denominador da
primeira das eqs.~\eqref{eq:frsnlp} cresce indefinidamente, ou seja, o
coeficiente de reflxão $E_{rp}/E_{ip}$ anula-se: a onda refletida está, assim,
totalmente polarizada na direção perpendicular ao plano de incidência, ou seja,
paralela ao plano de separação. O valor do ângulo de incidência para o qual esta
condição de polarização total do raio refletido se verifica chama-se
\emph{ângulo de Brewster} ($\theta_B$) para a interface dos dois meios. É fácil
verificar que, se $\theta_B+\theta_2=\pi/2,$ então os raios refletido e
refratado são perpendiculares. Desta perpendicularidade resulta imediatamente a
expressão, nesta altura do semestre já familiar, para o valor do ângulo de
Brewster:
\begin{equation}
  \tan\theta_B=\frac{n_2}{n_1}.
\end{equation}


\section{Apêndice --- Independência linear das exponenciais}
As igualdades da eq.~\eqref{eq:bdrconds} têm a forma genérica
\begin{equation}\label{eq:a10}
  A_1e^{\alpha_1x}+
  A_2e^{\alpha_2x}+
  A_3e^{\alpha_3x}+\ldots+
  A_ne^{\alpha_nx}=0,
\end{equation}
com constantes (que podem ser complexas) $A_1,\ A_2,\ \ldots,\ A_n$, $a_1,\
a_2,\ \ldots,\ a_n$, e $x$ real num intervalo dado $I$. Fixados $\alpha_1,\
\alpha_2,\ \ldots,\ \alpha_n$, esta igualdade traduz uma equação nas incógnitas
$A_1$, $A_2,\ \ldots,\ A_n$ que deve ser satisfeita para todo $x\in I$. Uma
solução óbvia, mas pouco interessante, é a chamada solução trivial 
\begin{equation*}
  A_1=A_2=\ldots=A_n=0\qquad\text{(solução trivial)}.
\end{equation*}
Haverá outras soluções, não triviais, desta equação?  Vamos demonstrar que pode
haver, mas só se os fatores $\alpha_k,\ k=1,\ldots,\ n$ forem todos
iguais.\footnote{Outra maneira de exprimir isto é dizer que funções exponenciais
  $e^{\alpha x}$ com coeficientes $\alpha$ diferentes são linearmente
independentes. Essa é a justificação para o título que dei a este apêndice.}

Consideremos então a eq.~\eqref{eq:a10} com fatores exponenciais $\alpha_1,\
\alpha_2,\ \ldots\, \alpha_n$ constantes complexas independentes de $x$ todas
diferentes\footnote{Duas exponenciais com fatores $\alpha$ iguais poderiam ser
agrupadas, redefinindo-se o coeficiente $A$ respetivo. Supor que são todas
diferentes não representa, assim, uma redução da generalidade da análise.} mas,
mais de resto, arbitrárias. Derivando a igualdade~\eqref{eq:a10} e
subtraindo-lhe o resultado do seu produto por $\alpha_1$, resulta
\begin{equation*}
  (\alpha_2-\alpha_1)A_2e^{\alpha_2x}+
  (\alpha_3-\alpha_1)A_3e^{\alpha_3x}+\ldots+
  (\alpha_n-\alpha_1)A_ne^{\alpha_nx}=0.
\end{equation*}
Repetimos agora uma operação semelhante com esta igualdade: derivamo-la e
subtraimos-lhe o seu produto com $\alpha_2$, obtendo-se
\begin{equation*}
  (\alpha_3-\alpha_2)(\alpha_3-\alpha_1)A_3e^{\alpha_3x}+\ldots+
  (\alpha_n-\alpha_2)(\alpha_n-\alpha_1)A_ne^{\alpha_nx}=0.
\end{equation*}
Repetindo este procedimento $n-1$ vezes no total, vamos eliminando
sucessivamente os vários termos, resultando por fim uma equação para o último da
soma,
\begin{equation*}
  (\alpha_n-\alpha_{n-1})
  (\alpha_n-\alpha_{n-2})\ldots
  (\alpha_n-\alpha_2)
  (\alpha_n-\alpha_1)A_ne^{\alpha_nx}=0.
\end{equation*}
Como $e^{\alpha_nx}\neq0$ e, por hipótese, os $\alpha_k$ são todos diferentes
(logo, também $\alpha_n-\alpha_k\neq0,\ \forall k$), resulta necessariamente
$A_n=0$. Podemos portanto descartar a última parcela na soma da
eq.~\eqref{eq:a10}. Repetindo novamente o processo com esta soma reduzida
concluímos agora que $A_{n-1}=0$ e, sucessivamente, que todos os coeficientes
$A_k$ são igualmente nulos. 
Ou seja, as combinações lineares de funções exponenciais $e^{\alpha_kx}$ com
fatores $\alpha_k$ diferentes só se anulam se os seus coeficientes forem
\emph{todos} nulos. Ou seja ainda, as funções exponenciais com fatores
$\alpha_k$ diferentes são linearmente independentes.  Assim, a  igualdade da
eq.~\eqref{eq:a10} só pode ser satisfeita com com coeficientes $A_k$ não todos
nulos se os fatores $\alpha_k$ nas exponenciais forem todos iguais.

\section{Apêndice --- Truque nas expressões da polarização ``P''}
\label{sect:dphi}
Nas eqs.~\eqref{eq:frsnlq} aparecem expressões com a forma
\begin{equation*}
  A = n_2\cos\theta_1\pm n_1\cos\theta_2.
\end{equation*}
Mas, de acordo com a lei de Snell, também tempos
\begin{equation*}
  n_1\sin\theta_1=n_2\sin\theta_2 \implies n_2 =
  n_1\frac{\sin\theta_1}{\sin\theta_2}.
\end{equation*}
Substituindo em cima, resulta
\begin{equation*}
  A=\frac{n_1}{\sin\theta_2}
  \left(\sin\theta_1\cos\theta_1\pm\sin\theta_2\cos\theta_2\right).
\end{equation*}
Agora, multipliquemos o primeiro entre parêntesis por 
$1=\cos^2\theta_2+\sin^2\theta_2$ e o segundo por
$1=\cos^2\theta_1+\sin^2\theta_1$. Reordenando os quatro termos, obtem-se
\begin{align*}
  A&=\frac{n_1}{\sin\theta_2}
    \left(
      \sin\theta_1\cos\theta_1\cos^2\theta_2 \pm
      \sin\theta_2\cos\theta_2\cos^2\theta_1 +
      \sin\theta_1\cos\theta_1\sin^2\theta_2 \pm
      \sin\theta_2\cos\theta_2\sin^2\theta_1 
    \right)\\
   &=\frac{n_1}{\sin\theta_2}
     \left(\strut\sin\theta_1\cos\theta_2 \pm \cos\theta_1\sin\theta_2\right)
     \left(\strut\cos\theta_1\cos\theta_2 \pm \sin\theta_1\sin\theta_2\right)\\
   &=\frac{n_1}{\sin\theta_2}
     \sin(\theta_1 \pm \theta_2)\cos(\theta_1 \mp \theta_2).
\end{align*}
Assim, e por exemplo para o coeficiente de reflexão, temos
\begin{align*}
  \left(\frac{E_{rp}}{E_{ip}}\right)_\text{P} &=
  \frac{n_2\cos\theta_1-n_1\cos\theta_2}%
  {n_2\cos\theta_1+n_1\cos\theta_2}=
  \frac{\sin(\theta_1-\theta_1)\cos(\theta_1+\theta_2)}%
  {\sin(\theta_1+\theta_1)\cos(\theta_1-\theta_2)}=
  \frac{\tan(\theta_1-\theta_2)}{\tan(\theta_1+\theta_2)}.
\end{align*}

